﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var FruitVege = new Dictionary<string, string>();
            var answer = "";

            Console.WriteLine(q1);
            answer = Console.ReadLine();
            var x = Dict(answer);
            Console.WriteLine(x);
            Console.ReadLine();
        }

        static string q1 = $"What sort of fruits for Vegetables are you looking for?"; 
        //static string q2 = $"The item you have chosen is {answer} which is a {FruitVege[answer]}"
        static string Dict(string answer)
        {
            string C = "";
            var FruitVege = new Dictionary<string, string>();
            FruitVege.Add("orange", "Is a Fruit and is in the dictionary");
            FruitVege.Add("pineapple", "Is a Fruit and is in the dictionary");
            FruitVege.Add("apple", "Is a Fruit and is in the dictionary");
            FruitVege.Add("peaches", "Is a Fruit and is in the dictionary");
            FruitVege.Add("peas", "Is a Vegetable and is in the dictionary");
            FruitVege.Add("corn", "Is a Vegetable and is in the dictionary");
            FruitVege.Add("pumpkin", "Is a Vegetable and is in the dictionary");
            FruitVege.Add("carrot", "Is a Vegetable and is in the dictionary");

            if ((answer == "orange") || (answer == "pineapple") || (answer == "apple") || (answer == "peaches") || (answer == "peas") || (answer == "corn") || (answer == "pumpkin") || (answer == "carrot"))
            {
                C = FruitVege[answer];
            }

            else
            {
                C = "Your item is not here!";
            }
            return C;
            
        }
    }
}
