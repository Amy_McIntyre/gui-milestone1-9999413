﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_05_Universal
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        static Random Number = new Random();
        static double lives = 5;
        static double userscores = 0;


        public MainPage()
        {
            this.InitializeComponent();
            textBlock3.Text = "Please enter a number from 1 to 5";
            textBlock1.Text = $"Score: {userscores} Lives:{lives}";
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            double userinput;

            userinput = double.Parse(textBox.Text);
            var z = output(userinput);
            textBlock1.Text = $"Score: {userscores} Lives:{lives}";

            if (lives == 0)
            {
                textBlock4.Text = $"Woops you are out of lives. You total end score was {userscores}";
                
            }
        }
        static double output(double userinput)
        {
            var x = 0;
            int RandNum = Number.Next(1, 5);

            if (RandNum == userinput)
            {
                userscores++;
                lives--;
            }
            else
            {
                lives--;
            }

            return x;

        }
    }
}
