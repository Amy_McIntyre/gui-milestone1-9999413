﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_01_Windows
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label2.Text = q1;
            label3.Text = q2;
        }

        private void convertbtn_Click(object sender, EventArgs e)
        {
            int choice;
            double userinput;

            choice = int.Parse(textBox1.Text);
            userinput = double.Parse(option.Text);
            var choices = choose(userinput, choice);
            answer.Text = $"{convert}{choices}";


        }
        static string q1 = $"Please enter a number you wish to be converted";
        static string q2 = $"1) Conversion of KM to Miles\n2) Conversion of Miles to KM";
        static string convert = "Number after Conversion:";

        static double choose(double userinput, int choice)
        {
            double a = 0;
            double total;

            switch (choice)
            {
                case 1:
                    total = userinput;
                    total = total * 1.609344;
                    a = total;
                    break;

                case 2:
                    total = userinput;
                    total = total * 0.62137119;
                    a = total;
                    break;
            }
            return a;


        }
    }
}
    

