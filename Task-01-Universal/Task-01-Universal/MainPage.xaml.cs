﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_01_Universal
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            textBlock2.Text = q1;
            textBlock1.Text = q2;
        }

        private void convertbtn_Click(object sender, RoutedEventArgs e)
        {
            int choice;
            double userinput;

            choice = int.Parse(userinput1.Text);
            userinput = double.Parse(useroption.Text);
            var choices = choose(userinput, choice);
            conversion.Text = $"{convert}{choices}";

        }
        static string q1 = $"Please enter a number you wish\n to be converted";
        static string q2 = $"1) Conversion of KM to Miles\n2) Conversion of Miles to KM";
        static string convert = "Number after Conversion: ";
        static double choose(double userinput, int choice)
        {
            double a = 0;
            double total;

            switch (choice)
            {
                case 1:
                    total = userinput;
                    total = total * 1.609344;
                    a = total;
                    break;

                case 2:
                    total = userinput;
                    total = total * 0.62137119;
                    a = total;
                    break;
            }
            return a;


        }
    }
}
