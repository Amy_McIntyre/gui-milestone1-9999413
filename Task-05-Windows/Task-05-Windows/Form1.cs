﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_05_Windows
{
    public partial class Form1 : Form
    {
        static Random Number = new Random();
        static double lives = 5;
        static double userscores = 0;


        public Form1()
        { 
            InitializeComponent();
        }

        private void guessbtn_Click(object sender, EventArgs e)
        {
            double userinput;
            label3.Text = $"Score: {userscores} Lives:{lives}";
            userinput = double.Parse(textBox1.Text);
            var z = output(userinput);
            label3.Text = $"Score: {userscores} Lives:{lives}";

            if(lives == 0)
            {
                MessageBox.Show($"Woops you are out of lives. You total end score was {userscores}");
                Application.Exit();
            }
        }
        static double output(double userinput)
        {
            var x = 0;
            int RandNum = Number.Next(1, 5);

            if (RandNum == userinput)
            { 
                userscores++;
                lives--;
            }
            else
            {
                lives--;
            }

            return x;

        }
    }
}

