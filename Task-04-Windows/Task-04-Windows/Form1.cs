﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_04_Windows
{
    public partial class Dictionary : Form
    {
        public Dictionary()
        {
            InitializeComponent();
            label1.Text = q1;
        }

        private void searchbtn_Click(object sender, EventArgs e)
        {
            var FruitVege = new Dictionary<string, string>();
            var answer = "";
            
            answer = searchbox.Text;
            var x = Dict(answer);
            answer1.Text = x;
        }

        static string q1 = $"What sort of fruits for Vegetables are you looking for?";
        static string Dict(string answer)
        {
            string C = "";
            var FruitVege = new Dictionary<string, string>();
            FruitVege.Add("orange", "Is a Fruit and is in the dictionary");
            FruitVege.Add("pineapple", "Is a Fruit and is in the dictionary");
            FruitVege.Add("apple", "Is a Fruit and is in the dictionary");
            FruitVege.Add("peaches", "Is a Fruit and is in the dictionary");
            FruitVege.Add("peas", "Is a Vegetable and is in the dictionary");
            FruitVege.Add("corn", "Is a Vegetable and is in the dictionary");
            FruitVege.Add("pumpkin", "Is a Vegetable and is in the dictionary");
            FruitVege.Add("carrot", "Is a Vegetable and is in the dictionary");

            if ((answer == "orange") || (answer == "pineapple") || (answer == "apple") || (answer == "peaches") || (answer == "peas") || (answer == "corn") || (answer == "pumpkin") || (answer == "carrot"))
            {
              C = FruitVege[answer];
            }

            else
            {
                C = "Your item is not here!";
            }
            return C;
        }
    }
    }

