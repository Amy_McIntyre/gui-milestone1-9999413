﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_04_Universal
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void search_Click(object sender, RoutedEventArgs e)
        {
            var FruitVege = new Dictionary<string, string>();
            var answer = "";

            answer = searchbox.Text;
            var x = Dict(answer);
            answer1.Text = x;
            answer = searchbox.Text;
        }



        static string q1 = $"What sort of fruits for Vegetables are you looking for?";
        static string Dict(string answer)
        {
            string C = "";
            var FruitVege = new Dictionary<string, string>();
            FruitVege.Add("orange", "Is a Fruit and is in the dictionary");
            FruitVege.Add("pineapple", "Is a Fruit and is in the dictionary");
            FruitVege.Add("apple", "Is a Fruit and is in the dictionary");
            FruitVege.Add("peaches", "Is a Fruit and is in the dictionary");
            FruitVege.Add("peas", "Is a Vegetable and is in the dictionary");
            FruitVege.Add("corn", "Is a Vegetable and is in the dictionary");
            FruitVege.Add("pumpkin", "Is a Vegetable and is in the dictionary");
            FruitVege.Add("carrot", "Is a Vegetable and is in the dictionary");

            if ((answer == "orange") || (answer == "pineapple") || (answer == "apple") || (answer == "peaches") || (answer == "peas") || (answer == "corn") || (answer == "pumpkin") || (answer == "carrot"))
            {
                C = FruitVege[answer];
            }

            else
            {
                C = "Your item is not here!";
            }
            return C;
        }
    }
}

