﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_03_Universal
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        static string q1 = "Please Enter your name";
        static string q2 = "Please Enter your age";
        public MainPage()
        {
            this.InitializeComponent();
            textBlock.Text = q1;
            textBlock1.Text = q2;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            var name = "";
            int age = 0;

            name = textBox.Text;

            age = int.Parse(textBox1.Text);
            var result = output(age, name);
            textBlock2.Text = result;
        }
        static string output(int age, string name)
        {
            string x = "";
            if (age >= 18)
            {
                x = $"Hello {name}, you are in Tertiary Study";
            }
            else if ((age >= 17) || (age >= 12))
            {
                x = $"Hello {name}, You are at Secondary School";
            }
            else if ((age <= 12))
            {
                x = $"Hello {name}, You are at Primary School or not at school";
            }
            return x;
        }
    }
}
