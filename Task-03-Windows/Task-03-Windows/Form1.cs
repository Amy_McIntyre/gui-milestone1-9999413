﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_03_Windows
{
    public partial class Form1 : Form
    {
        static string q1 = "Please Enter your name";
        static string q2 = "Please Enter your age";

        public Form1()
        {
            InitializeComponent();
            label2.Text = q1;
            label3.Text = q2;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            var name = "";
            int age = 0;

            name = textBox1.Text;

            age = int.Parse(textBox2.Text);
            var result = output(age, name);
            label1.Text = result;
        }


        static string output(int age, string name)
            {
            string x = "";
            if (age >= 18)
            {
                x = $"Hello {name}, you are in Tertiary Study";
            }
            else if (age >= 17 || age >= 12)
            {
                x = $"Hello {name}, You are at Secondary School";
            }
            else if (age < 12)
            {
                x = $"Hello {name}, You are at Primary School or are not in school";
            }

            return x;
        }
        }
    }

